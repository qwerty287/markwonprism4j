package de.qwerty287.markwonprism4j;

import android.text.SpannableStringBuilder;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import io.noties.prism4j.Syntax;

public interface Prism4jTheme {

    @ColorInt
    int background();

    @ColorInt
    int textColor();

    void apply(
            @NonNull String language,
            @NonNull Syntax syntax,
            @NonNull SpannableStringBuilder builder,
            int start,
            int end
    );
}
