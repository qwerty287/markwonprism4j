# markwon-prism4j

Fork of https://github.com/noties/Markwon/tree/master/markwon-syntax-highlight that
uses https://codeberg.org/qwerty287/Prism4j.

For some information on the usage, you can take a look at the
[original docs](https://noties.io/Markwon/docs/v4/syntax-highlight/), only import paths have to be changed. Another
way is to take a look at the
[plugin test](https://codeberg.org/qwerty287/markwonprism4j/src/branch/main/app/src/androidTest/java/de/qwerty287/markwonprism4j/PluginTest.java),
which renders text into a `TextView` and evaluates the text then.

You can get this via [JitPack](https://jitpack.io/#org.codeberg.qwerty287/markwonprism4j), unfortunately release builds
are not working for some reason, please use commit builds instead.