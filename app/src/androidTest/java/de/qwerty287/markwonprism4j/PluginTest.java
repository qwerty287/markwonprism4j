package de.qwerty287.markwonprism4j;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import io.noties.markwon.Markwon;
import io.noties.prism4j.Prism4j;
import io.noties.prism4j.Syntax;
import io.noties.prism4j.Text;
import io.noties.prism4j.Visitor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class PluginTest {

    @Test
    public void pluginTest() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Prism4jThemeDefault theme = Prism4jThemeDefault.create();
        Prism4j prism4j = new Prism4j();
        SyntaxHighlightPlugin plugin = SyntaxHighlightPlugin.create(prism4j, theme);
        Markwon markwon = Markwon.builder(context).usePlugin(plugin).build();
        TextView textView = new TextView(context);
        for (TestCase i : new TestCase[]{
                new TestCase("java", "System.out.println(\"Hello World!\");"),
                new TestCase("kotlin", "fun main() {\n    println(\"Hello World!\")\n}"),
                new TestCase("python", "print(\"Hello World!\")")
        }) {
            markwon.setMarkdown(textView, i.toMD());
            SpannableString textViewContent = (SpannableString) textView.getText();

            prism4j.visit(
                    new Visitor() {
                        int queryStart = 2; // the text rendered into the TextView starts with a newline and a tabulator

                        @Override
                        protected void visitText(@NonNull Text text) {
                            queryStart += text.textLength();
                        }

                        @Override
                        protected void visitSyntax(@NonNull Syntax syntax) {
                            ForegroundColorSpan[] colorSpans = textViewContent.getSpans(queryStart, queryStart + syntax.textLength(), ForegroundColorSpan.class);
                            int color = theme.color(i.language, syntax.type(), syntax.alias());
                            if (color != 0 && colorSpans.length == 1) {
                                Assert.assertEquals(
                                        "Error on " + syntax.matchedString() + " with type " + syntax.type() + ":",
                                        color,
                                        colorSpans[0].getForegroundColor()
                                );
                            } else if (colorSpans.length != 1) {
                                throw new IndexOutOfBoundsException("wrong length of the color spans (length=" + colorSpans.length + ")");
                            }
                            queryStart += syntax.textLength();
                        }
                    },
                    i.content, i.language
            );
        }
    }

    private static class TestCase {
        private final String language;
        private final String content;

        private TestCase(String language, String content) {
            this.language = language;
            this.content = content;
        }

        private String toMD() {
            return "```" + language + "\n" + content + "\n```";
        }
    }
}